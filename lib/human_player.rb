class HumanPlayer
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where"
    pos = gets.chomp
    return [pos.split.first.to_i,pos.split.to_a.last.to_i]
  end

  def display(board)
    board.grid.each do |row|
      row.each do |el|
        if el == nil
          print "_ "
        else
          print "#{el} "
        end
      end
      puts
    end
  end

end
