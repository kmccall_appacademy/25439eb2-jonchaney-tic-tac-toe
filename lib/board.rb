class Board
  attr_reader :grid

  def initialize(grid=[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    return true if grid[pos.first][pos.last] == nil
    false
  end

  def winner
    return :X if won?(:X)
    return :O if won?(:O)
  end

  def over?
    return true if winner == :X || winner == :O
    return true if board_status?(Symbol) # full board?
    return false if board_status?(NilClass) # empty board?
    false
  end

  private

  def won?(mark)
    return true if won_row?(mark)
    return true if won_column?(mark)
    return true if won_left_diagonal?(mark)
    return true if won_right_diagonal?(mark)
    false
  end

  def won_row?(mark)
    grid.each do |pos|
      if pos.all? {|el| el == mark}
        return true
      end
    end
    false
  end

  def won_column?(mark)
    grid.transpose.each do |pos|
      if pos.all? {|el| el == mark}
        return true
      end
    end
    false
  end

  def won_left_diagonal?(mark)
    grid[0][0] == mark && grid[1][1] == mark && grid[2][2] == mark
  end

  def won_right_diagonal?(mark)
    grid[0][2] == mark && grid[1][1] == mark && grid[2][0] == mark
  end

  def board_status?(status)
    flag = true
    grid.each do |row|
      next if row.all? {|mark| mark.class == status}
      flag = false  
    end
    flag
  end
end
